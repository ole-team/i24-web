(function ($, window, document) {
	'use strict';
	var domains = ['www.oim.news'];

    function make_item(obj,site){
    		var txttemp = (obj.title.rendered);
    		var li_item = $( '<li>' )
                .attr({'id':'id-vid-'+obj.id})
                .addClass('collection-item')
                .html('<span class="title">'+txttemp+'</span>');
                li_item.append('<a class="secondary-content actionCreate" data-id="'+obj.id+'" data-site="'+site+'" style="cursor:pointer;"><i class="material-icons">add_box</i></a>');
            li_item.click(function(){
                
            });
            
            li_item.children('a.actionCreate').click(function(){
            	$('.errormsg').hide();
            	var st = $(this).attr('data-site');
            	var ips = $(this).attr('data-id');
            	createpost(ips,st);
            });
            return li_item;
    };
    function initial_list(site){
            var urlsite = 'https://'+domains[site]+'/wp-json/wp/v2/posts?per_page=60';
            $.ajax( {
                type:'GET',
                url: urlsite,
                dataType:'json',
                success:function(data){
                    if( data.length ){
                        for( var i = 0; i < data.length; i += 1 ){
                            var temp_item = make_item(data[i],domains[site]);
                            $('#postmanage-list').append(temp_item);
                        }
                    }
                    
                }
            });
    };
    function createpost(id,sitio){
    		$('.content_posts').addClass('processing');
            var urli = 'insert.php';
            var params = {
                idpostmanager: id,
                domain : sitio
            }
            $.ajax( {
                type:'POST',
                url: urli,
                data: params,
                dataType:'json',
                success:function(data){
                	var temp_suf = 'id-vid-';
                	if(data!=-2){
                        var htmlURL = '<p><a href="'+data.url+'" target="_blank" class="waves-effect waves-light btn blue-grey darken-1">Editar<i class="material-icons right">send</i></a></p>';
                        $('li#'+temp_suf+id+' span').append(htmlURL);
                        $('li#'+temp_suf+id+' a.actionCreate').fadeOut('fast');
                		$('.content_posts').removeClass('processing');
                		$('.msg').removeClass('fail');
                		$('.msg').addClass('success').text('El post fue importado con éxito').fadeIn();
                	}else{
                		$('.content_posts').removeClass('processing');
                		$('.msg').removeClass('success');
                		$('.msg').addClass('fail').text('Ya existe una nota en el sitio creada con ese título').fadeIn();
                	}
                }
            });
    };
    $(document).ready(function(){
    	initial_list(0);
  	});
    

}(jQuery, window, document));

