<?php
/**
Template Name: Authors
*/
get_header();
the_post();

$single = new \JNews\Single\SinglePage();
?>

<div class="jeg_main <?php $single->main_class(); ?>">

    <div class="jeg_container">
        <div class="jeg_content jeg_singlepage">
            <div class="container">
                <div class="row">
                    <div class="jeg_main_content col-md-<?php echo esc_attr($single->column_width()) ?>">
                <?php if(jnews_can_render_breadcrumb()) : ?>
                    <div class="jeg_breadcrumbs jeg_breadcrumb_container">
                        <?php $single->render_breadcrumb(); ?>
                    </div>
                <?php endif; ?>

                    <div class="entry-header">
                        <h1 class="jeg_post_title"><?php the_title(); ?></h1>
                    </div>
                    <div class="entry-content <?php $single->share_float_additional_class() ?>">
                        <div class="content-inner" style="max-width: 1000px; margin: 0px auto;">
                            <?php 
                            $listUsers = array(3,7,8,27,2,4);
                            foreach ($listUsers as $user) {
                                $author_name = get_the_author_meta('user_firstname', $user);
                                $author_lastname = get_the_author_meta('user_lastname', $user);
                                $author_bio = get_the_author_meta('description', $user);
                                $author_link = get_author_posts_url($user);
                                //if(!empty($author_bio)){ ?>
                                <div class="post_info_author_bottom" style="border-top: none; border-bottom: none;"> 
                                <?php
                                $avatarlink = get_avatar($user);
                                if(empty($avatarlink)){
                                    echo '<div class="avatar_author"><img src="https://image.i24mujer.com/wp-content/uploads/2021/08/avatar_generico-150x150.jpeg" /></div>';
                                }else{
                                    echo '<div class="avatar_author">'.get_avatar($user, '150' ).'</div>';
                                }
                                echo '<div class="col_info_author"><a href="'.$author_link.'"><h2>'.$author_name.' '.$author_lastname.'</h2></a><p>'.$author_bio.'</p></div>';
                                ?>
                                    
                                </div>
                                
                            <?php //}
                            }
                            ?>
                            <div class="post_info_author_bottom" style="border-top: none; border-bottom: none;"> 
                                <div class="avatar_author"><img alt="" src="https://image.i24mujer.com/wp-content/uploads/2021/08/camila_hidalgo-150x150.jpg" srcset="https://image.i24mujer.com/wp-content/uploads/2021/08/camila_hidalgo-150x150.jpg" class="avatar avatar-150 photo" height="150" width="150" loading="lazy" data-pin-no-hover="true"></div><div class="col_info_author"><h2>Camila Hidalgo</h2><p>Publicista, con experiencia en postproducción audiovisual y animación 2D para contenidos en redes sociales y plataformas digitales.</p></div>
                            </div>
                            <div class="post_info_author_bottom" style="border-top: none; border-bottom: none;"> 
                                <div class="avatar_author"><img alt="" src="https://image.i24mujer.com/wp-content/uploads/2021/08/freddy_nunez-150x150.jpg" srcset="https://image.i24mujer.com/wp-content/uploads/2021/08/freddy_nunez-150x150.jpg" class="avatar avatar-150 photo" height="150" width="150" loading="lazy" data-pin-no-hover="true"></div><div class="col_info_author"><h2>Freddy Nuñez</h2><p>Artista digital y diseñador gráfico con 5 años de experiencia en la especialidad de branding, 3D design y motion design. Creador y docente en lettering en la escuela de diseño: Instituto de Diseño de Caracas (IDC).</p></div>
                            </div>
                            <style type="text/css">
                                    .post_info_author_bottom{
                                        color: #131313;
                                        display: flex;
                                        padding: 30px 15px;
                                    }
                                    .post_info_author_bottom .avatar_author {
    padding: 0px 15px; width: 20%;
}
.post_info_author_bottom .avatar_author img {
    border-radius: 90px;
    width: 100%;
}
.post_info_author_bottom .col_info_author {
    padding: 0px 30px;
    width: 80%;
}
                                </style>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <?php do_action('jnews_after_main'); ?>
    </div>
</div>

<?php get_footer(); ?>